# YunQ_DynamicResume

#### 介绍<br><br>
1. 基于HTML+css的多动症个人简历。开箱即用。
2. 演示地址：https://www.guoqingyun.top/dynamicResume.html<br><br> 

#### 技术难点<br><br>
1. 字符串的提取、拼接。要注意字符串拼接的时候，往styleTag里面吐的代码，一旦包含注释//，要对/进行转义。
2. setinterval定时器控制整个代码的吞吐节奏/速率。
3. 字符串命名一定要切实合理。<br><br>
#### 项目截图<br><br>
![Image text](https://gitee.com/guo_zi_shan/yun-q_-dynamic-resume/raw/master/images/1.png)

![Image text](https://gitee.com/guo_zi_shan/yun-q_-dynamic-resume/raw/master/images/2.png)

![Image text](https://gitee.com/guo_zi_shan/yun-q_-dynamic-resume/raw/master/images/3.png)
 